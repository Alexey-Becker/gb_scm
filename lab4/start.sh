#!/bin/bash

[[ ! -d "./ext_roles/geerlingguy.mysql" ]] && ansible-galaxy install geerlingguy.mysql

[[ ! -d "./ext_roles/geerlingguy.nginx" ]] && ansible-galaxy install geerlingguy.nginx

[[ ! -d "./ext_roles/geerlingguy.php" ]] && ansible-galaxy install geerlingguy.php

if [[ -d "./ext_roles/geerlingguy.mysql" && -d "./ext_roles/geerlingguy.nginx" && -d "./ext_roles/geerlingguy.php" ]]; then
    echo "All rolles are existists, start playbook."
    ansible-playbook pb-lab4.yml -K --vault-password-file ~/.ansible_pass.txt --tags wordpress
else
    echo "Required external roles does not exist, please check your scripts"
fi
